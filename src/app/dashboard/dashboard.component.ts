import { Component, OnInit } from '@angular/core';
import { MessageproviderService } from '../messageprovider.service'
import { Subscription } from 'rxjs';
import { catchError } from 'rxjs/operators';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import{AutocompleteComponent}from'../autocomplete/autocomplete.component'
declare var google
export interface CHILD {
  value: string;
  viewValue: string;
}
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  message: any;
  subscribe: Subscription;
  isLinear = false;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  location:string
  child: CHILD[] = [
    {value: '0', viewValue: '10-12'},
    {value: '1', viewValue: '12-15'},
    {value: '2', viewValue: '15-30'}
  ];
  constructor(public dialog:MatDialog,public messageServive: MessageproviderService,private _formBuilder: FormBuilder) {
  
    this.location='Select the location'
   



  }

  countrypopup()
  {
    const dialogRef = this.dialog.open(AutocompleteComponent, {
      width: '1050px',
      height:'500px',
      data:{location:this.location}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
  
    this.location=localStorage['location']
    });
  }


  ngOnInit() {
    var input = document.getElementById('locationTextField')
    // console.log('input',input)
    // this.mySpan.nativeElement.innerHTML
  
    var autocomplete = new google.maps.places.Autocomplete(input);
    this.firstFormGroup = this._formBuilder.group({
      firstCtrl: ['', Validators.required]
    });
    this.secondFormGroup = this._formBuilder.group({
      secondCtrl: ['', Validators.required]
    });
  }

}
