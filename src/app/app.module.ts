import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import {MatBottomSheetModule, MatBottomSheet} from '@angular/material/bottom-sheet';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import {MatSidenavModule} from '@angular/material/sidenav';
import { MediaMatcher } from '@angular/cdk/layout';
import { ChangeDetectorRef, Component, OnDestroy } from '@angular/core';
import {MatSelectModule} from '@angular/material/select';
import {MatDialogModule} from '@angular/material/dialog';
import {MatListModule,MatButtonModule,MatCheckboxModule,MatToolbarModule,MatInputModule,MatProgressSpinnerModule,MatCardModule,MatMenuModule, MatIconModule} from '@angular/material';
import { HomeComponent } from './home/home.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import{RouterModule}from'@angular/router'
import{routes}from'./routing_module';
import { ShowlistComponent } from './showlist/showlist.component';
import { LoginComponent } from './login/login.component';
import { LoginlayoutComponent } from './loginlayout/loginlayout.component';
import { NavigationComponent } from './navigation/navigation.component'
import {MatTabsModule} from '@angular/material/tabs';
import {HttpClientModule} from '@angular/common/http';
import { LoaderComponent } from './loader/loader.component';
import{HTTP_INTERCEPTORS}from'@angular/common/http'
import{LoaderinterceptorserviceService}from'./loaderinterceptorservice.service'
import { NgxSpinnerModule } from 'ngx-spinner';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import{AddauthService}from'./addauth.service'
import { FileSelectDirective } from 'ng2-file-upload';
import { TermsnconditionComponent } from './termsncondition/termsncondition.component';
import { DialogboxComponent } from './dialogbox/dialogbox.component';
import { NavigationagentComponent } from './navigationagent/navigationagent.component';
import {MatStepperModule} from '@angular/material/stepper';
import { AutocompleteComponent } from './autocomplete/autocomplete.component';
import { OrphangeComponent } from './orphange/orphange.component';
import { AgmCoreModule } from '@agm/core';
import { ProfileComponent } from './profile/profile.component';
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    DashboardComponent,
    ShowlistComponent,
    LoginComponent,
    LoginlayoutComponent,
    NavigationComponent,
    LoaderComponent,
    FileSelectDirective,
    TermsnconditionComponent,
    DialogboxComponent,
    NavigationagentComponent,
    AutocompleteComponent,
    OrphangeComponent,
    ProfileComponent
    
  ],

  imports: [
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAnnrs3LJlEkOP9hLnQ8_k2CBSAJ8--kZs'
    }),
    NgxSpinnerModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule,
    RouterModule.forRoot(routes),
    BrowserModule,
    BrowserAnimationsModule,
    MatButtonModule,
     MatCheckboxModule,
     BrowserModule,
      FormsModule,
      MatSidenavModule,MatTabsModule,MatBottomSheetModule,MatDialogModule,MatStepperModule,
   MatListModule,MatSelectModule,   MatButtonModule, MatCheckboxModule,MatToolbarModule,MatInputModule,MatProgressSpinnerModule,MatCardModule,MatMenuModule,MatIconModule
,MatSnackBarModule
  ],
  exports: [MatStepperModule,MatDialogModule,MatBottomSheetModule,MatSelectModule,MatTabsModule,MatListModule,MatButtonModule, MatCheckboxModule,MatToolbarModule,MatInputModule,MatProgressSpinnerModule,MatCardModule,MatMenuModule, MatIconModule],
  
  providers: [
    HttpClientModule,
    
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AddauthService,
      multi: true
    }
  ],
  entryComponents:[TermsnconditionComponent,DialogboxComponent,AutocompleteComponent]
  ,
  bootstrap: [AppComponent]
})
export class AppModule { }
