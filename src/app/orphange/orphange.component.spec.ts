import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrphangeComponent } from './orphange.component';

describe('OrphangeComponent', () => {
  let component: OrphangeComponent;
  let fixture: ComponentFixture<OrphangeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrphangeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrphangeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
