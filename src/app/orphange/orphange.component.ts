import { Component, OnInit } from '@angular/core';
import { MouseEvent } from '@agm/core';
declare var google
@Component({
  selector: 'app-orphange',
  templateUrl: './orphange.component.html',
  styleUrls: ['./orphange.component.css']
})
export class OrphangeComponent implements OnInit {
  selectedValue: number = 30000;
  currentLat: number ;
  currentLng: number ;
  markers: marker[] = [
    {
      lat: 51.673858,
      lng: 7.815982,
      label: 'A',
      draggable: false
    },
    {
      lat: 51.373858,
      lng: 7.215982,
      label: 'B',
      draggable: false
    },
    {
      lat: 51.723858,
      lng: 7.895982,
      label: 'C',
      draggable: false
    }
  ]
  constructor() {
    if (navigator)
    {
    navigator.geolocation.getCurrentPosition( pos => {
        this.lng = +pos.coords.longitude;
        this.lat = +pos.coords.latitude;
        console.log(this.lng+'--'+this.lat)
        this.markers.push({ lat: this.lat,
          lng: this.lng,
          label: 'C',
          draggable: false})
for(var i=0;i<4;i++)
{
this.markers.push({
  lat: this.lat+.3,
  lng: this.lng,
  label: 'C',
  draggable: false

})
}



      });
    }
   }

  ngOnInit() {

  }
  getDistance(data){
    console.log('data'+data);
    let newMarkers: marker[];
    console.log(this.distanceCalculator(this.markers[0].lat,this.markers[0].lng,this.currentLat,this.currentLng));
  }

 


  distanceInMiles = [
    { id : '1', distance: 20000},
    { id : '2', distance: 40000},
    { id : '3', distance: 60000},
    { id : '4', distance: 80000}
  ];

  // getLocation() {
  //   if (navigator.geolocation) {
  //     navigator.geolocation.getCurrentPosition(this.showPosition);
    
  //   } else {
  //     console.log("Geolocation is not supported by this browser.");
  //   }
  // }
  
  showPosition(position) {
    this.currentLat = position.coords.latitude;
    this.currentLng = position.coords.longitude; 
    console.log('lat'+this.currentLat)
  }

  distanceCalculator(lat1,lon1,lat2,lon2) {
      var R = 6371; // Radius of the earth in km
      var dLat = this.deg2rad(lat2-lat1);  // deg2rad below
      var dLon = this.deg2rad(lon2-lon1); 
      var a = 
        Math.sin(dLat/2) * Math.sin(dLat/2) +
        Math.cos(this.deg2rad(lat1)) * Math.cos(this.deg2rad(lat2)) * 
        Math.sin(dLon/2) * Math.sin(dLon/2)
        ; 
      var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
      var d = R * c; // Distance in km
      return d;
  }
    
  deg2rad(deg) {
    return deg * (Math.PI/180)
  }

 // google maps zoom level
 zoom: number = 8;
  
 // initial center position for the map
 lat: number = 51.673858;
 lng: number = 7.815982;

 clickedMarker(label: string, index: number) {
   console.log(`clicked the marker: ${label || index}`)
 }
 
 mapClicked($event: MouseEvent) {
   this.markers.push({
     lat: $event.coords.lat,
     lng: $event.coords.lng,
     draggable: true
   });
 }
 
 markerDragEnd(m: marker, $event: MouseEvent) {
   console.log('dragEnd', m, $event);
 }
 

}

// just an interface for type safety.
interface marker {
 lat: number;
 lng: number;
 label?: string;
 draggable: boolean;
}
