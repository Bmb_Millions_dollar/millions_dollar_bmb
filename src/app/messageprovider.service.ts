import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MessageproviderService {
  private subject = new BehaviorSubject<any>({});


  constructor() { }

  sendMessage(message: String) {
    console.log('message'+message)
    this.subject.next({ text: message });
  }
  clearMessage() {
    this.subject.next({});
  }

  getMessage(): Observable<any> {
    console.log('In get Message');
    return this.subject.asObservable();
  }




}
