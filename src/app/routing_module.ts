import{RouterModule,Routes}from'@angular/router'
import { HomeComponent } from './home/home.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NgModule } from '@angular/core';
import { LoginComponent } from './login/login.component';
import { ShowlistComponent } from './showlist/showlist.component';
import { LoginlayoutComponent } from './loginlayout/loginlayout.component';
import{OrphangeComponent}from'./orphange/orphange.component'
import{ProfileComponent}from'./profile/profile.component'
import { AuthguardService } from './authguard.service';
import{NavigationComponent}from'./navigation/navigation.component'

export const routes:Routes=[
    {path:'',component:LoginComponent,canActivate:[AuthguardService]},

    // { path: '', redirectTo: 'orphange', pathMatch: 'full' },
// {path:'login',component:LoginComponent,children:[
// {path:'',component:LoginComponent}

// ]


// },

// { path: '**', redirectTo: ''},
{
path:'home',component:HomeComponent
},

{
    path:'login',component:LoginComponent,
   
},
{
    path:'showlist',component:ShowlistComponent
},
{
    path:'orphange',component:OrphangeComponent
},
{
 path:'profile',component:ProfileComponent
},
{
path:'notes',component:DashboardComponent,
children:[

{path:'login',component:LoginComponent},
{path:'showlist',component:ShowlistComponent}

]
}
];





