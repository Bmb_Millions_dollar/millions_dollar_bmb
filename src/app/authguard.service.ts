import { Injectable } from '@angular/core';
import { 
  ActivatedRouteSnapshot, 
  CanActivate, 
  Router, 
  RouterStateSnapshot } from '@angular/router';
import{Observable}from'rxjs'
import{ServiceapiService}from'./serviceapi.service'
import {map ,take } from 'rxjs/operators';
  @Injectable(
    {
  providedIn: 'root'
}
)
export class AuthguardService implements CanActivate{

  constructor(private authService:ServiceapiService,private router:Router) { }
  canActivate(next :ActivatedRouteSnapshot,state:RouterStateSnapshot)
 : Observable<boolean>|Promise<boolean>|boolean
  
  {

return this.authService.isloggedIn.pipe(
take(1),
map((isLoggedIn:boolean)=>{

  if (!isLoggedIn){
    this.router.navigate(['/login']);  // {4}
    return false;
  }
  return true;



}) 





)
}




}
