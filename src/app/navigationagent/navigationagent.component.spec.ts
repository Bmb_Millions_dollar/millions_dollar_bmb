import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavigationagentComponent } from './navigationagent.component';

describe('NavigationagentComponent', () => {
  let component: NavigationagentComponent;
  let fixture: ComponentFixture<NavigationagentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavigationagentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavigationagentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
