import { Injectable } from '@angular/core';
import{Http,Headers,RequestOptions}from'@angular/http'
import{ENV}from'./env'
import { HttpClient } from '@angular/common/http';
import{map}from'rxjs/operators'
import{BehaviorSubject}from'rxjs';
import { Router } from '@angular/router';
import{MessageproviderService}from'./messageprovider.service';
@Injectable({
  providedIn: 'root'
})
export class ServiceapiService {
private loggedIn=new BehaviorSubject<boolean>(false);
  public getToken(): string {

    return localStorage['token'];
  }
  constructor(private http: HttpClient,public router:Router,public messageprovider:MessageproviderService) { }
get isloggedIn(){
  return this.loggedIn.asObservable();
}
 signup(
  Username,
  Email,
  Name,
  Surname,
  Password,role
 )
 {

   return this.http.post(ENV.mainApi+'user/signup', {
    
      email:Email,
      password:Password,
      username:Username,
      name:Name,
      surname:Surname,
      profile_pic:localStorage['image'],
      role:role
  
  
    }).pipe(map(user=>{
       

      return user;
    }))

 
  }


login(email,password,role)
{
  
  
  this.messageprovider.sendMessage(role)
if(role=='Agent')
{
  this.loggedIn.next(true);
  this.router.navigate(['notes'])
}
else if(role=='Parent')
{
  this.loggedIn.next(true);
this.router.navigate(['showlist'])
}
else
{
  this.loggedIn.next(true);
  this.router.navigate(['orphange'])
}





  // this.router.navigate(['showlist']);
// return this.http.post(ENV.mainApi+'user/login',{
//   email:email,
//   password:password
// }).pipe(map(user=>{
//   return user;
// }))
}

logout() {                            // {4}
this.loggedIn.next(false);
this.router.navigate(['/login']);
}







addchild()
{
  // var tok=localStorage['token']
  // let token1='Bearer'+tok
  // let headers = new Headers({ 'Content-Type':'application/json','Authorization':token1})

  // let requestOptions=new RequestOptions({headers:headers})
  return this.http.post(ENV.mainApi+'agents/addchild',{
    child_pic:"yatash",
    color:"yatash123",
    age:"yatash",
    name:"yatash",
    userid:"5bd062e365f5cae0e813ef78"
  }).pipe(map(user=>{
    return user;
  }))
}






  

}
