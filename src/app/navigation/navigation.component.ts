import { Component, OnInit } from '@angular/core';
import{MessageproviderService}from'../messageprovider.service'
import{Subscription}from'rxjs';
import { Observable } from 'rxjs';
import{LoginComponent}from'../login/login.component'
import{ServiceapiService}from'../serviceapi.service'
@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css'],
  providers:[LoginComponent]
})
export class NavigationComponent implements OnInit {
role:any;
subscribe:Subscription;
isLoggedIn$: Observable<boolean>
  constructor(private messageServive:MessageproviderService,public authService:ServiceapiService) {
 
    this.subscribe = this.messageServive.getMessage().subscribe(message => {
    
      console.log('In dashboard component', message.text);

      this.role = message.text;

    }, error => {
      console.log(error)
    })
   
   }
  onLogout(){
    
      this.authService.logout();                      // {3}
    
  }
   ngOnInit() {
    

    this.isLoggedIn$ = this.authService.isloggedIn; 
    alert(this.isLoggedIn$)
  }
  ngAfterViewInit()
  {
    
  }

}
