import { TestBed } from '@angular/core/testing';

import { LoaderinterceptorserviceService } from './loaderinterceptorservice.service';

describe('LoaderinterceptorserviceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LoaderinterceptorserviceService = TestBed.get(LoaderinterceptorserviceService);
    expect(service).toBeTruthy();
  });
});
