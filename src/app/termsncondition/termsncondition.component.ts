import { Component, OnInit } from '@angular/core';
import{MatBottomSheet,MatBottomSheetRef}from'@angular/material';
@Component({
  selector: 'app-termsncondition',
  templateUrl: './termsncondition.component.html',
  styleUrls: ['./termsncondition.component.css']
})
export class TermsnconditionComponent implements OnInit {

  constructor(private bottomSheetref:MatBottomSheetRef<TermsnconditionComponent>) { }

  ngOnInit() {
  }
  openlink(event:MouseEvent):void{
this.bottomSheetref.dismiss();
event.preventDefault();
}

}
