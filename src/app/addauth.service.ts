import { Injectable } from '@angular/core';
import{HttpRequest,HttpHandler,HttpEvent,HttpInterceptor}from'@angular/common/http'
import{Observable}from'rxjs'
import{ServiceapiService}from'../app/serviceapi.service'
@Injectable({
  providedIn: 'root'
})
export class AddauthService implements HttpInterceptor {

  constructor(public auth: ServiceapiService) {

  }
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
   
    console.log(this.auth.getToken())
    // req = req.clone({ headers: req.headers.set('Authorization', 'Bearer ' + authToken) });
    request = request.clone({
      setHeaders: {
      
        Authorization: 'Bearer '+localStorage['token']
       
      
      }
    });
    return next.handle(request);
  }
}
