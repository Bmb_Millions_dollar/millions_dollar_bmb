import { Component, ElementRef,OnInit } from '@angular/core';
import{Http,Headers,RequestOptions}from'@angular/http'
import{ServiceapiService}from'../serviceapi.service'
import{first}from'rxjs/operators'
import{ENV}from'../env'
import { HttpClient } from '@angular/common/http';
import{map}from'rxjs/operators'
import { NgxSpinnerService } from 'ngx-spinner';
import{FormBuilder,FormGroup,Validators}from'@angular/forms'
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatSnackBar} from '@angular/material';
 import { Router } from '@angular/router';
 import { FileSelectDirective, FileItem } from 'ng2-file-upload';
 import {  FileUploader } from 'ng2-file-upload/ng2-file-upload';
import{MatBottomSheet,MatBottomSheetRef}from'@angular/material';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import{TermsnconditionComponent}from'../termsncondition/termsncondition.component'
import{DialogboxComponent}from'../dialogbox/dialogbox.component';
import{state,trigger,transition,animate,style}from'@angular/animations'
import{MessageproviderService}from'../messageprovider.service';
import{mergeMap,debounceTime}from'rxjs/operators'
import{pipe, observable, fromEvent}from'rxjs'
import { BehaviorSubject } from 'rxjs';
var options:
 {fileKey: 'file',
 chunkedMode: false,
 mimeType: "multipart/form-data",
 params: { 'upload_preset': 'c5neyvgt' } 
 }




 export interface Role {
  value: string;
  viewValue: string;
}
 const URL = 'https://api.cloudinary.com/v1_1/loginworks/image/upload';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  animations: [
 
    //For the logo
    trigger('fadeIn', [
      state('in', style({
        opacity: 1
      })),
      transition('void => *', [
        style({opacity: 0}),
        animate('450ms 900ms ease-in')
      ])
 
    ])]
})

export class LoginComponent implements OnInit {
  loginState: any = "in";
  public loggedIn = new BehaviorSubject<boolean>(false);
  public uploader:FileUploader = new FileUploader({
    url: URL,
     itemAlias: 'photo'

  ,autoUpload: true,
  // Use xhrTransport in favor of iframeTransport
  isHTML5: true,
  // Calculate progress independently for each uploaded file
  removeAfterUpload: true,
  // XHR request headers
  headers: [
    {
      name: 'X-Requested-With',
      value: 'XMLHttpRequest'
    },

 

  ]

});
  Username
  Email
  Name
  Surname
  Password

  RegisterForm:FormGroup
  submitAttempt: boolean = false;
data
selectedFile:File=null;
roles: Role[] = [
  {value: 'Agent', viewValue: 'Agent'},
  {value: 'Parent', viewValue: 'Parent'},
  {value: 'Orphanage', viewValue: 'Orphanage'}
];
animal: string;
name: string;
public check
dialogRef
  constructor(public messageservice:MessageproviderService,public dialog: MatDialog,private bottomSheet:MatBottomSheet,private el: ElementRef,public router: Router,public snackBar: MatSnackBar,public formBuilder:FormBuilder,private spinner: NgxSpinnerService,public serviceapi:ServiceapiService,public http: HttpClient) {
    let emailRegex =/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
    let passwordRegex = /^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{6,}$/;

localStorage['hope']='hi'
this.RegisterForm=formBuilder.group({
  Username:['', Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
  Email:['',Validators.compose([Validators.maxLength(50), Validators.pattern(emailRegex), Validators.required])],
  Name:['', Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
  Surname:['', Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
  Password:['',Validators.compose([Validators.maxLength(30), Validators.pattern(passwordRegex), Validators.required])],
 role:['', Validators.compose([Validators.pattern('[a-zA-Z]*'), Validators.required])],
 check: ['', Validators.required]
})



   }
   mergemapdemo()
{

}
ngAfterViewInit()
{
  const input1 = document.getElementById('example1');
  const input2=document.getElementById('example2')
const example1=fromEvent(input1,'keyup')
const example2=fromEvent(input2,'keyup')

example1.pipe(mergeMap(i1=>{
return example2.pipe(map(
  i2=>(i1.target as HTMLInputElement).value+' '+(i2.target as HTMLInputElement).value
))
})).subscribe(val=>{
console.log(val)
})
  // const example1 = fromEvent(input1, 'keyup').pipe(map(i =>
    
  
  //   (i.currentTarget as HTMLInputElement).value
 
  // ));
  // const example2=fromEvent(input2,'keyup').pipe(mergeMap(i=>
  //   (i.currentTarget as HTMLInputElement).value
    
    
  // ))
  //wait .5s between keyups to emit current value
  //throw away all other values

//    const textinput =example1.pipe(mergeMap(event1=>{
//      return example2.pipe(map(
//  event2=>event1['target'].value+''+event2['target'].value
//      )

//      )
//    })).subscribe(val=>{
// console.log(val)
//    }
     
//    )
  // const debouncedInput = example1.pipe(mergeMap(event=>{
  //   return event
  // }))
  
  //log values
  // const subscribe = textinput.subscribe(val => {
  //   console.log(`Debounced Input: ${val}`);
  // });
}
sendMessage():void{

  this.messageservice.sendMessage('HIII')
 
  this.router.navigate(['notes'])
}

clearMessage():void{
  this.messageservice.clearMessage();
}
 
   openDialog(): void {
     this.dialogRef = this.dialog.open(DialogboxComponent, {
      width: '350px',
      data: {name: this.name, animal: 'hihih'}
    });

    this.dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    console.log('result'+result)
    this.register()
    });
  }



   onfileSelected(event)
   {
     this.selectedFile=<File>event.target.files[0];
   }
   onChkChange($event)
   {
    console.log($event.checked);
this.check=$event.checked;
if(this.check){
this.bottomSheet.open(TermsnconditionComponent);
}
else{

}
   }


   ngOnInit() {
   
  
    this.uploader.onAfterAddingFile = (file)=> {
      
       file.withCredentials = false; 
      
    
    };
    this.uploader.onBuildItemForm=(file:any,form:FormData):any=>{
      form.append('upload_preset','u3ngppa3')
      form.append('file',file)

     
    
      file.withCredentials = false; 
      return{file,form};
    };

    this.uploader.onCompleteItem = (item:any, response:any, status:any, headers:any) => {
         console.log("ImageUpload:uploadeKKd:", JSON.stringify(item));
     };
 }






  register()
  {
   
//     this.spinner.show();
// this.serviceapi.signup(
//   this.RegisterForm.controls["Username"].value,
//   this.RegisterForm.controls["Email"].value,
//   this.RegisterForm.controls["Name"].value,
//   this.RegisterForm.controls["Surname"].value,
//   this.RegisterForm.controls["Password"].value,
//   this.RegisterForm.controls["role"].value
// ).subscribe(data=>{


//   this.spinner.hide();
//   this.snackBar.open('SignIn  Succesfully','', {
//     duration: 2000,
//   });


// this.Email=this.RegisterForm.controls["Email"].value
// this.Password= this.RegisterForm.controls["Password"].value


  this.login()

// });

  }

  onfileselected(event)
  {
  this.selectedFile=<File>event.target.files[0]
  }
  onUpload()
  {

  const fd=new FormData();
  fd.append('file',this.selectedFile,this.selectedFile.name)
  fd.append('upload_preset','wjnegjnc')
  fd.append('itemAlias', 'photo')
  fd.append('Content-Type','application/json')
  this.http.post('https://api.cloudinary.com/v1_1/loginworks/image/upload',fd)
  .subscribe(res=>{
   
    console.log(res)
  });
  
  }
  
  

   


login()
{
 
//   this.spinner.show()
//  this.serviceapi.login(
//    this.Email,this.Password
//  ).subscribe(data=>{
// this.spinner.hide()
// this.snackBar.open('Login Succesfully','',{
//   duration:2000
// })
// this.data=data
// this.data.token
// console.log('data'+data)
//piu@gmail.com
//yatash123
// localStorage['token']=this.data.token
// console.log(localStorage['token'])
// this.messageservice.sendMessage('HIII')
// console.log('parent'+this.data.role)
// this.messageservice.sendMessage(this.data.role)
// alert(this.RegisterForm.controls["role"].value)
// this.messageservice.sendMessage(this.RegisterForm.controls["role"].value)
// this.router.navigate(['notes'])

// if(this.RegisterForm.controls["role"].value=='Agent')
// {
//   this.loggedIn.next(true);
//   this.router.navigate(['notes'])
// }
// else if(this.RegisterForm.controls["role"].value=='Parent')
// {
//   this.loggedIn.next(true);
// this.router.navigate(['showlist'])
// }
// else
// {
//   this.loggedIn.next(true);
//   this.router.navigate(['orphange'])
// }






// }) 


 this.serviceapi.login(this.Email,this.Password,this.RegisterForm.controls["role"].value)



}











}
