import { Component, OnInit,ViewChild, ElementRef } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import{ServiceapiService}from'../serviceapi.service'
import{ENV}from'../env'
import { NgxSpinnerService } from 'ngx-spinner';
declare var google;
export interface Food {
  value: string;
  viewValue: string;
}
@Component({
  selector: 'app-showlist',
  templateUrl: './showlist.component.html',
  styleUrls: ['./showlist.component.css']
})

export class ShowlistComponent implements OnInit {
  showFiller = false;
  foods: Food[] = [
    {value: 'steak-0', viewValue: 'Steak'},
    {value: 'pizza-1', viewValue: 'Pizza'},
    {value: 'tacos-2', viewValue: 'Tacos'}
  ];
@ViewChild('map')mapElement:ElementRef
map:any;
lat
lng
childlist=[{id:1,details:'hghgh'},
{id:1,details:'hghgh'},
{id:1,details:'hghgh'},
{id:1,details:'hghgh'},
{id:1,details:'hghgh'},
{id:1,details:'hghgh'},
{id:1,details:'hghgh'},
{id:1,details:'hghgh'},
{id:1,details:'hghgh'},
{id:1,details:'hghgh'},
{id:1,details:'hghgh'},
{id:1,details:'hghgh'},
{id:1,details:'hghgh'},
{id:1,details:'hghgh'},
{id:1,details:'hghgh'},
{id:1,details:'hghgh'}
]
  constructor(private spinner: NgxSpinnerService,public serviceapi:ServiceapiService) { 
navigator.geolocation.getCurrentPosition((success)=>{
  console.log('sucess'+success)
  console.log('lat',success.coords.latitude)
  console.log('lng',success.coords.longitude)


  this.lat=success.coords.latitude
  this.lng=success.coords.longitude
  this.loadmap()
  
}
  
  
  
  ,error=>{
    console.log('error'+error)
  })


  }

  ngOnInit() {
  
  }
  loadmap()
  {
    let latlng=new google.maps.LatLng(this.lat,this.lng)
    let mapOptions={
      center:latlng,
      zoom:15,
      mapTypeId:google.maps.MapTypeId.ROADMAP
    }
    this.map=new  google.maps.Map(this.mapElement.nativeElement,mapOptions)
  }

addchild()
{

  this.spinner.show();
  this.serviceapi.addchild().subscribe(data=>{
  
  
    this.spinner.hide();
  
  
  
  
  });

  
}
addMarker()
{

}


}
