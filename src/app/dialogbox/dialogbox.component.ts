import { Component, OnInit,Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import{FormBuilder,FormGroup,Validators}from'@angular/forms'
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import{MatBottomSheet,MatBottomSheetRef}from'@angular/material';
import { HttpClient } from '@angular/common/http';
import { NgxSpinnerService } from 'ngx-spinner';
import {MatSnackBar} from '@angular/material';
export interface DialogData {
  animal: string;
  name: string;
}
@Component({
  selector: 'app-dialogbox',
  templateUrl: './dialogbox.component.html',
  styleUrls: ['./dialogbox.component.css']
})
export class DialogboxComponent implements OnInit {
selectedFile:File;
ProfileImage=''
upload
url
  constructor(public snackBar:MatSnackBar,public dialogRef: MatDialogRef<DialogboxComponent>, @Inject(MAT_DIALOG_DATA) public data: DialogData,public http: HttpClient,private spinner: NgxSpinnerService) {
this.data.animal


    }

  onNoClick(): void {
    this.dialogRef.close();

  }

  ngOnInit() {
  }
  onFileChanged(event)
  {
  this.selectedFile=<File>event.target.files[0];
  alert('ss'+this.selectedFile.size)
var reader=new FileReader();  
reader.readAsDataURL(event.target.files[0]);
reader.onload=(event)=>{
  this.ProfileImage=(<FileReader>event.target).result;
 
}

}
  onUpload()
  {
    this.spinner.show();
  const fd=new FormData();
  fd.append('file',this.selectedFile,this.selectedFile.name)

  fd.append('upload_preset','wjnegjnc')
  fd.append('itemAlias', 'photo')
  fd.append('Content-Type','application/json')
  this.http.post('https://api.cloudinary.com/v1_1/loginworks/image/upload',fd)
  .subscribe(res=>{

    this.spinner.hide();
    this.snackBar.open('Image Uploaded Successfully!!','', {
      duration: 2000,
    });
   this.url=res
localStorage['image']=this.url.url
this.dialogRef.close();


    
  });
  
  }

}
