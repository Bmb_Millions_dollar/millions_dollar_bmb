import { Component, OnInit } from '@angular/core';
import{LoaderState}from'../loaderstate'
import { Subscription } from 'rxjs';
// import { LoaderService } from '../_services/loader.service';
import{LoaderserviceService}from'../loaderservice.service'
@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.css']
})
export class LoaderComponent implements OnInit {

  show = false;
  private subscription: Subscription;
  constructor(private loaderService: LoaderserviceService) { }
  ngOnInit() {
    this.subscription = this.loaderService.loaderState
    .subscribe((state: LoaderState) => {
      this.show = state.show;
    });
  }
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
